# Guide for eshop-ms-basket
## Pull from DockerHub


```bash
docker pull episenba/eshop-ms-basket:1.0.0-RELEASE
```

## Docker commands
Run container
```bash
docker run -d -p 8060:8060 --name ms-basket eshop-ms-basket:1.0.0-RELEASE
```
Check logs
```bash
docker logs ms-basket
```
Stop container
```bash
docker stop ms-basket
```
Remove container
```bash
docker container rm ms-basket
```
## Logs

**Check logs to monitor server execution and see actions like *JWToken* validation or management of the basket repository.**

## Usage

####Create basket with Authentication

> POST request

http://localhost:8060/episen/osd/basket/api/v1/authenticate/validate
> BODY
```json
{
	"value": "XXX.XXX.XXX"
}
```
> Will authenticate the member thanks to the token put in the `value` field. Will create a empty basket with the member username contained in the token. 
> 
> Check logs or http response to retrieve the JWT ***token*** created at authentication on [eshop-ms-membership](https://gitlab.com/boisseaualexis/osd/eshop-ms-membership/-/blob/c144956fcfba8fcdd006995569d4bb5a9bd946ec/README.md)

----------
####Put product in member's basket : only with USER authority

> POST request

http://localhost:8060/episen/osd/basket/api/v1/baskets/alexis/products
> BODY
```json
{
	"token": "XXX.XXX.XXX",
	"gtin": "XXX",
}
```
> All fields are required. 
> Check logs for products **gtin** created at initialisation or or see section *####See all products* of this file or :
>
> Use **PRD10** to add PRD10 product to basket
>
> Use **PRD20** to see the behaviour when the product quantity is 0


----------
####See all baskets
> GET request

http://localhost:8060/episen/osd/basket/api/v1/baskets/

----------
####See one basket
> GET request

http://localhost:8060/episen/osd/basket/api/v1/baskets/{username}

----------
####See one basket's products
> GET request

http://localhost:8060/episen/osd/basket/api/v1/baskets/{username}/products


----------
####See all products
> GET request

http://localhost:8060/episen/osd/basket/api/v1/products

----------
####See one product
> GET request

http://localhost:8060/episen/osd/basket/api/v1/products{gtin}



