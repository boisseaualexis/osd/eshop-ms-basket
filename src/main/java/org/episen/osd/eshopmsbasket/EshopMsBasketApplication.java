package org.episen.osd.eshopmsbasket;

import org.episen.osd.eshopmsbasket.security.JWTValidator;
import org.episen.osd.eshopmsbasket.service.BasketService;
import org.episen.osd.eshopmsbasket.service.ProductService;
import org.episen.osd.eshopmsbasket.settings.PopulateBasketRepo;
import org.episen.osd.eshopmsbasket.settings.PopulateProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class EshopMsBasketApplication  implements CommandLineRunner {

	@Autowired
	private JWTValidator validator;

	@Autowired
	private ProductService productService;

	@Autowired
	private BasketService basketService;

	public static void main(String[] args) {

		SpringApplication.run(EshopMsBasketApplication.class, args);
		System.out.println("BASKET MICROSERVICE STARTED");
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("BASKET MICROSERVICE INITIALISATION");

		PopulateProductRepo populateProductRepo = new PopulateProductRepo(productService, "/json/products.json");
		System.out.println("POPULATING PRODUCTS IN MEMORY");
		populateProductRepo.populate();
		System.out.println("DONE POPULATING PRODUCTS IN MEMORY");

		/*PopulateBasketRepo populateBasketRepo = new PopulateBasketRepo(basketService, "");
		populateBasketRepo.populate();*/
	}

}
