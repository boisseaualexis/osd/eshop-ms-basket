package org.episen.osd.eshopmsbasket.service;

import org.episen.osd.eshopmsbasket.model.ProductModel;
import org.episen.osd.eshopmsbasket.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository repository;

    public void addProduct(ProductModel product){
        if((null==(product.getGtin())) || (product.getGtin().isBlank())){
            throw new RuntimeException("Error : Product not added");
        }
        if(repository.getProductByGtin(product.getGtin())!=null){
            throw new RuntimeException("Exception : Product already exist");
        }
        repository.addProduct(product);
    }

    public ProductModel getProduct(String gtin){

        ProductModel product = repository.getProductByGtin(gtin);

        if(product == null){
            throw new RuntimeException("Get Exception : Product not found");
        }

        return product;
    }

    public List<ProductModel> getAll(){
        return repository.getAll();
    }

    public void updateProduct(String gtin, ProductModel product){

        if(repository.getProductByGtin(gtin) == null) {
            throw new RuntimeException("Update Error : Product not found");
        }
        if(product.getGtin() == null) {
            product.setGtin(gtin);
        }

        repository.updateProduct(product);
    }

    public void deleteProduct(String gtin){

        if(repository.getProductByGtin(gtin) == null){
            throw new RuntimeException("Delete Error : Product not found");
        }

        repository.deleteProductByGtin(gtin);
    }

    public ProductRepository getRepository() {
        return repository;
    }

    public void setRepository(ProductRepository repository) {
        this.repository = repository;
    }

    public void deleteOneProduct(String gtin) {
        repository.deleteOneProductByGtin(gtin);
    }

    public void addOneProduct(String gtin) {
        repository.addOneProductByGtin(gtin);
    }
}
