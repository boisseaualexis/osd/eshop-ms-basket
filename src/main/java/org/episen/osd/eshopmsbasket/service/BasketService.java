package org.episen.osd.eshopmsbasket.service;

import org.episen.osd.eshopmsbasket.model.BasketModel;
import org.episen.osd.eshopmsbasket.model.ProductModel;
import org.episen.osd.eshopmsbasket.model.UserContext;
import org.episen.osd.eshopmsbasket.repository.BasketRepository;
import org.episen.osd.eshopmsbasket.security.JWTValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BasketService {

    @Autowired
    private JWTValidator jwtValidator;

    @Autowired
    private BasketRepository repository;

    @Autowired
    private ProductService productService;

    public void addProductIntoBasket(String token, String gtin, String usernameToCheck) {

        if ((productService.getProduct(gtin)==null)){
            throw new RuntimeException("Error : Product does not exist");
        }

        if((token==null) || (token.isBlank())){
            throw new RuntimeException("Error : Please put a valid token. Product not added in basket");
        }

        if (!(productService.getProduct(gtin).getQuantity()>0)){
            throw new RuntimeException("Exception : Product not added : None available");
        }

        UserContext user = jwtValidator.transform(token);
        String username = user.getSubject();
        List<String> authorities = user.getAuthorities();

        if(username.isBlank() || (username==null)
                || (authorities.isEmpty()) || (authorities==null)
                || !(authorities.contains("USER"))
                || !username.equals(usernameToCheck)){
            throw new RuntimeException("Exception : Product not added : Member not allowed to add product");
        }

        ProductModel product = new ProductModel();
        product.setGtin(productService.getProduct(gtin).getGtin());
        product.setQuantity(productService.getProduct(gtin).getQuantity());
        product.setLabel(productService.getProduct(gtin).getLabel());
        product.setPrice(productService.getProduct(gtin).getPrice());

        if(repository.getBasketByMemberUsername(username).getProducts().containsKey(gtin)){
            repository.addMoreProductIntoBasket(username, product);
        } else {
            repository.addProductIntoBasket(username, product);
        }

        System.out.println("Quantity in stock before delete : " + productService.getProduct(gtin).getQuantity());
        productService.deleteOneProduct(gtin);
        System.out.println("Quantity in stock after delete : " + productService.getProduct(gtin).getQuantity());
    }

    /*public void deleteProductIntoBasket(String token, String gtin, String usernameToCheck){
        if((token==null) || (token.isBlank()) || (gtin==null) || (gtin.isBlank())){
            throw new RuntimeException("Error : Product not added in basket");
        }

        UserContext user = jwtValidator.transform(token);
        String username = user.getSubject();
        List<String> authorities = user.getAuthorities();

        if(username.isBlank() || (username==null)
                || (authorities.isEmpty()) || (authorities==null)
                || !(authorities.contains("USER"))
                || !username.equals(usernameToCheck)){
            throw new RuntimeException("Exception : Member not allowed to add product");
        }

        if(repository.getBasketByMemberUsername(username).getProducts().containsKey(gtin)){
            repository.deleteOneProductFromBasket(username, gtin);
        }else {
            repository.deleteProductFromBasket(username, gtin);
        }
        productService.addOneProduct(gtin);
    }*/

    public void addBasket(BasketModel basket){
        if((null==(basket.getMemberUsername())) || (basket.getMemberUsername().isBlank())){
            throw new RuntimeException("Error : Basket not added");
        }
        if(repository.getBasketByMemberUsername(basket.getMemberUsername())!=null){
            throw new RuntimeException("Exception : Basket already exist");
        }
        repository.addBasket(basket);
    }

    public BasketModel getBasket(String username){

        BasketModel basket = repository.getBasketByMemberUsername(username);

        if(basket == null){
            throw new RuntimeException("Get Exception : Basket not found");
        }

        return basket;
    }

    public List<BasketModel> getAll(){
        return repository.getAll();
    }

    public void updateBasket(String username, BasketModel basket){

        if(repository.getBasketByMemberUsername(username) == null) {
            throw new RuntimeException("Update Error : Basket not found");
        }
        if(basket.getMemberUsername() == null) {
            basket.setMemberUsername(username);
        }

        repository.updateBasket(basket);
    }

    public void deleteBasket(String username){

        if(repository.getBasketByMemberUsername(username) == null){
            throw new RuntimeException("Delete Error : Basket not found");
        }

        repository.deleteBasketByMemberUsername(username);
    }

    public BasketRepository getRepository() {
        return repository;
    }

    public void setRepository(BasketRepository repository) {
        this.repository = repository;
    }

    public void createBasketFromMembershipMSAuthentication(String username) {
        repository.createBasketFromMembershipMSAuthentication(username);
    }
}