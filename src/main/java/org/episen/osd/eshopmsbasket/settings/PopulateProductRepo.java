package org.episen.osd.eshopmsbasket.settings;

import org.episen.osd.eshopmsbasket.model.ProductModel;
import org.episen.osd.eshopmsbasket.service.ProductService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class PopulateProductRepo {

    private String filePath;

    private JSONParser jsonParser;

    private ProductService service;

    public PopulateProductRepo(ProductService service, String filePath) {
        this.filePath = filePath;
        this.jsonParser = new JSONParser();
        this.service = service;
    }

    public void populate(){

        try {
            InputStream is = PopulateProductRepo.class.getResourceAsStream(this.filePath);
            //System.out.println(is.available());
            //File file = resource.getFile();
            //FileReader reader = new FileReader("/json/products.json");

            Object obj = new JSONParser().parse(new InputStreamReader(is, "UTF-8"));
            JSONArray productList = (JSONArray) obj;

            //Iterate over product array
            productList.forEach( product -> parseProductModel( (JSONObject) product ) );

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

    }

    private void parseProductModel(JSONObject product)
    {
        ProductModel productToAdd = new ProductModel();

        //Get product gtin
        String gtin = (String) product.get("gtin");
        productToAdd.setGtin(gtin);

        //Get product label
        String label = (String) product.get("label");
        productToAdd.setLabel(label);

        //Get product price
        Long priceLong = (Long) product.get("price");
        productToAdd.setPrice(priceLong.intValue());
        
        //Get product quantity
        Long quantityLong = (Long) product.get("quantity");
        productToAdd.setQuantity(quantityLong.intValue());

        /*//Get product authorities
        List<String> authorities = (ArrayList<String>) product.get("authorities"); //new ArrayList<>();
        /*JSONArray authoritiesList = (JSONArray) product.get("authorities");
            //Iterate over product authorities array
        authoritiesList.forEach( auth -> authoritiesList.add(auth) );*/
        //productToAdd.setAuthorities(authorities);

        service.addProduct(productToAdd);
    }
}
