package org.episen.osd.eshopmsbasket.repository;

import org.episen.osd.eshopmsbasket.model.BasketModel;
import org.episen.osd.eshopmsbasket.model.ProductModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class BasketRepository {

    private Map<String, BasketModel> basketInMemory = new HashMap<>();

    public void addBasket(BasketModel basket){
        System.out.println("AddBasket , username : " + basket.getMemberUsername());
        basketInMemory.put(basket.getMemberUsername(), basket);
    }

    public BasketModel getBasketByMemberUsername(String username){
        System.out.println("GetBasketByMemberUsername , username : " + username);
        return basketInMemory.get(username);
    }

    public List<BasketModel> getAll(){
        return new ArrayList<BasketModel>(basketInMemory.values());
    }

    public void updateBasket(BasketModel basket){
        System.out.println("UpdateBasket , username :" + basket.getMemberUsername());
        basketInMemory.put(basket.getMemberUsername(), basket);
    }

    public void deleteBasketByMemberUsername(String username){
        System.out.println("DeleteBasketByMemberUsername , username : " + username);
        basketInMemory.remove(username);
    }

    public void addMoreProductIntoBasket(String username, ProductModel product) {
        System.out.println("AddMoreProductIntoBasket , username : " + username + ", product : " + product.toString());
        int quantityInBasket = basketInMemory.get(username).getProducts().get(product.getGtin()).getQuantity();
        System.out.println("Quantity in basket : " + quantityInBasket + ", Quantity to set : " + quantityInBasket+1);
        System.out.println("Quantity of temp "+ product.getGtin() + ", BEFORE : " + product.getQuantity());
        product.setQuantity(quantityInBasket+1);
        System.out.println("Quantity of temp "+ product.getGtin() +", AFTER : " + product.getQuantity());
        System.out.println("Quantity of "+ product.getGtin() +" in " + username + "'s basket, BEFORE : " + basketInMemory.get(username).getProducts().get(product.getGtin()).getQuantity());
        basketInMemory.get(username).getProducts().put(product.getGtin(), product);
        System.out.println("Quantity of "+ product.getGtin() +" in " + username + "'s basket, AFTER : " + basketInMemory.get(username).getProducts().get(product.getGtin()).getQuantity());
    }

    public void addProductIntoBasket(String username, ProductModel product) {
        System.out.println("AddProductIntoBasket , username : " + username + ", product : " + product.toString());
        System.out.println("Quantity of temp "+ product.getGtin() +", BEFORE : " + product.getQuantity());
        product.setQuantity(1);
        System.out.println("Quantity of temp "+ product.getGtin() +", AFTER : " + product.getQuantity());
        basketInMemory.get(username).getProducts().put(product.getGtin(), product);
        System.out.println("Quantity of "+ product.getGtin() +" in " + username + "'s basket, AFTER : " + basketInMemory.get(username).getProducts().get(product.getGtin()).getQuantity());
    }

    public void deleteOneProductFromBasket(String username, String gtin) {
        System.out.println("DeleteOneProductIntoBasket , username : " + username + ", product : " + gtin);
        int quantityInBasket = basketInMemory.get(username).getProducts().get(gtin).getQuantity();
        basketInMemory.get(username).getProducts().get(gtin).setQuantity(quantityInBasket-1);
    }

    public void deleteProductFromBasket(String username, String gtin) {
        System.out.println("DeleteProductIntoBasket , username : " + username + ", product : " + gtin);
        basketInMemory.get(username).getProducts().remove(gtin);
    }

    public void createBasketFromMembershipMSAuthentication(String username) {
        System.out.println("createBasketFromMembership , username : " + username);
        BasketModel basket = new BasketModel();
        Map<String, ProductModel> products = new HashMap<>();
        basket.setMemberUsername(username);
        basket.setProducts(products);
        basketInMemory.put(basket.getMemberUsername(), basket);
    }
}
