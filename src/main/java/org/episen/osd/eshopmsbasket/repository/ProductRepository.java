package org.episen.osd.eshopmsbasket.repository;

import org.episen.osd.eshopmsbasket.model.ProductModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ProductRepository {

    private Map<String, ProductModel> productInMemory = new HashMap<>();

    public void addProduct(ProductModel product){
        System.out.println("AddProduct : " + product.toString());
        productInMemory.put(product.getGtin(), product);
    }

    public ProductModel getProductByGtin(String gtin){
        System.out.println("GetProductByGtin , gtin : " + gtin);
        return productInMemory.get(gtin);
    }

    public List<ProductModel> getAll(){
        return new ArrayList<ProductModel>(productInMemory.values());
    }

    public void updateProduct(ProductModel product){
        System.out.println("UpdateProduct , product : " + product.toString());
        productInMemory.put(product.getGtin(), product);
    }

    public void deleteProductByGtin(String gtin){
        System.out.println("DeleteProductByGtin , gtin : " + gtin);
        productInMemory.remove(gtin);
    }

    public void deleteOneProductByGtin(String gtin) {
        Integer quantity = productInMemory.get(gtin).getQuantity()-1;
        if(!(quantity>0)){
            System.out.println("Error : deleteOneProductByGtin : None available");
        }
        productInMemory.get(gtin).setQuantity(quantity);
    }

    public void addOneProductByGtin(String gtin) {
        Integer quantity = productInMemory.get(gtin).getQuantity()+1;
        productInMemory.get(gtin).setQuantity(quantity);
    }
}
