package org.episen.osd.eshopmsbasket.model;

public class ProductModel {

    private String gtin;

    private String label;

    private Integer price;

    private Integer quantity;

    public String getGtin() {
        return gtin;
    }

    public void setGtin(String gtin) {
        this.gtin = gtin;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "ProductModel{" +
                "gtin='" + gtin + '\'' +
                ", label='" + label + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
