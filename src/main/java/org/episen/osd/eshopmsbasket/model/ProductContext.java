package org.episen.osd.eshopmsbasket.model;

import java.util.List;

public class ProductContext {

    private String token;

    private String gtin;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getGtin() {
        return gtin;
    }

    public void setGtin(String gtin) {
        this.gtin = gtin;
    }
}
