package org.episen.osd.eshopmsbasket.model;

import java.util.HashMap;
import java.util.Map;

public class BasketModel {

    private String memberUsername;

    private Map<String, ProductModel> products = new HashMap<>();

    public String getMemberUsername() {
        return memberUsername;
    }

    public void setMemberUsername(String memberUsername) {
        this.memberUsername = memberUsername;
    }

    public Map<String, ProductModel> getProducts() {
        return products;
    }

    public void setProducts(Map<String, ProductModel> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "BasketModel{" +
                "member='" + memberUsername + '\'' +
                ", products=" + products +
                '}';
    }
}
