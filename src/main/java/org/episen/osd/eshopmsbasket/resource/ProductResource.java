package org.episen.osd.eshopmsbasket.resource;

import org.episen.osd.eshopmsbasket.model.ProductModel;
import org.episen.osd.eshopmsbasket.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "products", produces = {"application/json"})
public class ProductResource {

    @Autowired
    private ProductService service;

    @GetMapping
    public List<ProductModel> getAll(){
        return service.getAll();
    }

    @GetMapping("{gtin}")
    public ProductModel getOne(@PathVariable("gtin") String gtin){

        return service.getProduct(gtin);
    }

    @PostMapping()
    public void addProduct(@RequestBody ProductModel product){

        service.addProduct(product);
    }

    @PutMapping("{gtin}")
    public void updateProduct(@PathVariable("gtin") String gtin, @RequestBody ProductModel product){

        service.updateProduct(gtin, product);
    }

    @DeleteMapping("{gtin}")
    public void deleteProduct(@PathVariable("gtin") String gtin){

        service.deleteProduct(gtin);
    }
}
