package org.episen.osd.eshopmsbasket.resource;

import org.episen.osd.eshopmsbasket.model.SingleValue;
import org.episen.osd.eshopmsbasket.model.UserContext;
import org.episen.osd.eshopmsbasket.security.JWTValidator;
import org.episen.osd.eshopmsbasket.service.BasketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "authenticate", produces = {"application/json"})
public class AuthenticateResource {
	
	@Autowired
	private JWTValidator jwtValidator;

	@Autowired
	private BasketService basketService;

	@PostMapping("validate")
	@ResponseStatus(value = HttpStatus.ACCEPTED)
	public UserContext parseToken(@RequestBody SingleValue value){
		UserContext userContext = jwtValidator.transform(value.getValue());

		basketService.createBasketFromMembershipMSAuthentication(userContext.getSubject());

		return userContext;
	}
}
