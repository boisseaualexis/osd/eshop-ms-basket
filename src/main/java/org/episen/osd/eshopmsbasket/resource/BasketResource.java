package org.episen.osd.eshopmsbasket.resource;

import org.episen.osd.eshopmsbasket.model.BasketModel;
import org.episen.osd.eshopmsbasket.model.ProductContext;
import org.episen.osd.eshopmsbasket.model.ProductModel;
import org.episen.osd.eshopmsbasket.service.BasketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "baskets", produces = {"application/json"})
public class BasketResource {

    @Autowired
    private BasketService service;

    @GetMapping
    public List<BasketModel> getAll(){
        return service.getAll();
    }

    @GetMapping("{username}")
    public BasketModel getOne(@PathVariable("username") String username){

        return service.getBasket(username);
    }

    @GetMapping("{username}/products")
    public Map<String, ProductModel> getProductsFromOne(@PathVariable("username") String username){

        return service.getBasket(username).getProducts();
    }

    @PostMapping()
    public void addBasket(@RequestBody BasketModel basket){

        service.addBasket(basket);
    }

    @PostMapping("{username}/products")
    public void addProductIntoBasket(@PathVariable("username") String username, @RequestBody ProductContext productContext){

        service.addProductIntoBasket(productContext.getToken(), productContext.getGtin(), username);
    }

    @PutMapping("{username}")
    public void updateBasket(@PathVariable("username") String username, @RequestBody BasketModel basket){

        service.updateBasket(username, basket);
    }

    @DeleteMapping("{username}")
    public void deleteBasket(@PathVariable("username") String username){

        service.deleteBasket(username);
    }

    /*@DeleteMapping("{username}/products")
    public void deleteBasket(@PathVariable("username") String username, @RequestBody ProductContext productContext){

        service.deleteProductIntoBasket(productContext.getToken(), productContext.getGtin(), username);
    }*/
}
